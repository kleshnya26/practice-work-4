def print_error():
	print('Failed! Try again!')

def try_again():
	print_error()
	main()

def is_correct(N):
    if N <= 0:
    	raise ValueError

def test_is_correct():
	assert is_correct(3) is None
	try:
		assert is_correct(-5) == ValueError
		assert is_correct(0) == ValueError
	except:
		pass

def test_task():
	assert task([5, 4, 2, 7, 6, 7, 2]) == '2->4->7'
	assert task([4, 3, 2, 1]) == 'Nope'

def task(numbers):
	a, b, c = 0, 0, 0
	for e in range(len(numbers)):
		if e+1 == numbers[e]:
			continue
		a = numbers[e-1]
		b = numbers[a-1]
		c = numbers[b-1]
		if c == e:
			return str(a) + '->' + str(b) + '->' + str(c)
	return 'Nope'

def main():
	try:
		N = int(input())
		is_correct(N)
	except ValueError:
		try_again()

	try:
		numbers = [e for e in map(int, input().split())]
	except ValueError:
		try_again()
	if len(numbers) != N:
		try_again()
	for n in numbers:
		if n < 1:
			try_again()
		if n > N:
			try_again()

	print(task(numbers))

if __name__ == '__main__':
	main()